// Exponent Operator
const getCube = x => x ** 3;


// Template Literals
let num = 4;
console.log(`The cube of ${num} is ${getCube(num)}`);


// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

const [number, street, city, zip] = address;
console.log(`I live at ${number} ${street}, ${city} ${zip}`);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const { name, species, weight, measurement } = animal;
console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${measurement}.`);


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
numbers.forEach(num => console.log(num));


// Javascript Classes
class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let myDog = new Dog("Goldie", 3, "Golden Retriever");
let yourDog = new Dog("Brownie", 5, "Aspin");
console.log(myDog);
console.log(yourDog);